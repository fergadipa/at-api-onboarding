const ep1 = require('../endpoints/endPoint1.js');
const tc = require('../cases/testCase1.js');

let response;

describe(tc.describe, () => {
  test(tc.testcase.test1.title, async () => {
    response = await ep1.getSingleUser(2);
    expect(response.status, 'Ini harus response 200').toBe(tc.testcase.test1.responseStatus);
  });

  test(tc.testcase.test2.title, async () => {
    response = await ep1.getSingleUser(23);
    expect(response.status, 'Ini harus response 404').toBe(tc.testcase.test2.responseStatus);
  });
});
