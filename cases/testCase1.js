const rs = require('./../common/responseStatusLibrary.js');

const testcase = {
  describe: 'Test',
  test1: {
    title: 'get a single user use id',
    responseStatus: rs.responseStatus.statusOk,
  },
  test2: {
    title: 'single user not found',
    responseStatus: rs.responseStatus.statusNotFound,
  },
};

module.exports = {
  testcase,
};
