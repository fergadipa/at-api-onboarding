const responseStatus = {
  statusOk: 200,
  statusCreated: 201,
  statusBadRequest: 400,
  statusForbidden: 403,
  statusUnauthorized: 401,
  statusNotFound: 404,
};

module.exports = {
  responseStatus,
};
