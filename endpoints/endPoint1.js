/* eslint no-unused-vars: off */
/* global env */
const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.API_BASE_URL);

const getSingleUser = id => api.get(`/users/${id}`)
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

module.exports = {
  getSingleUser,
};
